import javazoom.jl.player.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ResourceBundle;

public class Main extends JFrame implements ActionListener {

    private final static String[] LANGUAGES = {"PL", "EN", "CS"};
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");

    private JTextField textField;
    private JComboBox language;
    private JButton button;
    private Image windowIcon;

    public Main() throws HeadlessException {
        windowIcon = new ImageIcon(Main.class.getResource("say.png")).getImage();

        boolean trayInUse = false;
        if (SystemTray.isSupported()) {
            try {
                setupTrayIcon();
                trayInUse = true;
            } catch (Exception e) {
            }
        }

        setDefaultCloseOperation(trayInUse ? WindowConstants.HIDE_ON_CLOSE : WindowConstants.EXIT_ON_CLOSE);
        setIconImage(windowIcon);
        setLayout(new FlowLayout());

        language = new JComboBox(LANGUAGES);
        textField = new JTextField(20);
        textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    Main.this.setVisible(false);
                }
            }
        });
        button = new JButton(resourceBundle.getString("say.button.label"));
        textField.addActionListener(this);
        button.addActionListener(this);

        setTitle(resourceBundle.getString("window.title"));
        getContentPane().add(language);
        getContentPane().add(textField);
        getContentPane().add(button);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);

        textField.requestFocusInWindow();
        getRootPane().setDefaultButton(button);
    }

    private void setupTrayIcon() throws AWTException, MalformedURLException {
        PopupMenu popup = new PopupMenu();

        MenuItem sayMenuItem = new MenuItem(resourceBundle.getString("say.menu.label"));
        sayMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.this.setVisible(true);
            }
        });
        popup.add(sayMenuItem);

        MenuItem exitMenuItem = new MenuItem(resourceBundle.getString("exit.menu.label"));
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        popup.add(exitMenuItem);

        TrayIcon trayIcon = new TrayIcon(windowIcon, resourceBundle.getString("tray.popup.label"), popup);
        trayIcon.setImageAutoSize(true);
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Main.this.setVisible(true);
            }
        });

        SystemTray systemTray = SystemTray.getSystemTray();
        systemTray.add(trayIcon);
    }

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        parseProxySettings();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        });
    }

    private static void parseProxySettings() {
        System.setProperty("java.net.useSystemProxies", "true");
        String httpProxy = System.getenv("HTTP_PROXY");
        if (httpProxy != null) {
            try {
                URL url = new URL(httpProxy);
                int port = url.getPort();
                if (port == -1) {
                    port = 80;
                }
                final String host = url.getHost();
                final String userInfo = url.getUserInfo();

                System.setProperty("http.proxyHost", host);
                System.setProperty("http.proxyPort", "" + port);
                if (userInfo != null) {
                    final String[] tokens = userInfo.split(":");
                    System.setProperty("http.proxyUser", tokens[0]);
                    System.setProperty("http.proxyPassword", tokens[1]);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        MessageFormat.format(resourceBundle.getString("proxy.error.message"), e.getClass().getName(), e.getMessage()),
                        resourceBundle.getString("window.title"), JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        }
    }

    public void say(final String text, final String language) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                HttpURLConnection connection = null;
                InputStream inputStream = null;
                Player player = null;
                try {
                    String encodedText = URLEncoder.encode(text, "UTF-8");
                    String ulr =
                            String.format("http://translate.google.com/translate_tts?tl=%s&q=%s", language, encodedText);
                    URL url = new URL(ulr);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent",
                            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 ( .NET CLR 3.5.30729)");
                    connection.setRequestProperty("Accept", "audio/mpeg");
                    inputStream = new BufferedInputStream(connection.getInputStream());
                    player = new Player(inputStream);
                    player.play();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null,
                            MessageFormat.format(resourceBundle.getString("say.error.message"), e.getClass().getName(), e.getMessage()),
                            resourceBundle.getString("window.title"), JOptionPane.OK_OPTION);
                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                    } catch (IOException e) {
                    }
                    if (connection != null) {
                        connection.disconnect();
                    }
                    if (player != null) {
                        player.close();
                    }
                }
            }
        };
        new Thread(runnable).start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final String text = textField.getText();
        if (text != null && text.length() > 0) {
            say(text, language.getSelectedItem().toString().toLowerCase());
        }
    }
}
